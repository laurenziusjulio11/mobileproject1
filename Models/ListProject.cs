namespace MobileProject

{
    public class ListProject
    {
        public string Detail { get; set; }
        public int Count { get; set; }
        public string ImageSource { get; set; }
    }
}