﻿namespace MobileProject
{
    public class Project
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public string Details { get; set; }
        public string Image { get; set; }
    }
}
