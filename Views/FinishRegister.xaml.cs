namespace MobileProject.Views;

using Mopups.Services;



public partial class FinishRegister
{
    public FinishRegister()
    {
        InitializeComponent();
    }


    private async void FinishButton_Clicked(object sender, EventArgs e)
    {
        MopupService.Instance.PopAsync();
        await Navigation.PushAsync(new HomePage());

    }






}