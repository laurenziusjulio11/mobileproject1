namespace MobileProject.Views;

public partial class Repassword : ContentPage
{
	public Repassword()
	{
		InitializeComponent();
	}

    private async void ConfirmChangePassword_Clicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new HomePage());
    }
}