namespace MobileProject.Views;

using Mopups.Services;



public partial class EmailAlreadyUsedPopUp
{
    public EmailAlreadyUsedPopUp()
    {
        InitializeComponent();
    }


    private async void EmailDuplicateButton_Clicked(object sender, EventArgs e)
    {
        MopupService.Instance.PopAsync();
        await Navigation.PushAsync(new RegisterPage());

    }






}