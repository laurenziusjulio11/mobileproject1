using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json.Serialization;
using Mopups.Services;
using MobileProject.ViewModels;


namespace MobileProject.Views
{
    public partial class JoinMember : ContentPage
    {
        public JoinMember()
        {
            InitializeComponent();

            // Create an instance of JoinMemberViewModel and set it as the BindingContext
            BindingContext = new JoinMemberViewModel();

            // Load data when the page appears
            this.Appearing += async (sender, e) => await LoadDetailProjects();
        }

        private async Task LoadDetailProjects()
        {
            try
            {
                var viewModel = (JoinMemberViewModel)BindingContext;

                var httpClient = new HttpClient();
                var responseStream = await httpClient.GetStreamAsync("https://gitlab.com/bootcamp-batch-7/mobileproject/-/raw/main/projectdataPDetail.json?ref_type=heads");

                // Deserialize the JSON data into a list of DetailProject
                var detailProjects = await System.Text.Json.JsonSerializer.DeserializeAsync<List<DetailProject>>(responseStream);

                if (detailProjects != null)
                {
                    // Update the DetailProjects property of the ViewModel
                    viewModel.DetailProjects.Clear();
                    foreach (var project in detailProjects)
                    {
                        viewModel.DetailProjects.Add(project);
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions, e.g., network errors
                Console.WriteLine($"Error loading detail projects: {ex}");
            }
        }

        private void ConfirmationButton_Clicked(object sender, EventArgs e)
        {
            MopupService.Instance.PushAsync(new DetailJobConfirmation());
        }
    }
}
