using Mopups.Services;

namespace MobileProject.Views;

public partial class DetailJobConfirmation
{
	public DetailJobConfirmation()
	{
		InitializeComponent();
	}

    private void DetailConfirmationNo_Clicked(object sender, EventArgs e)
    {
        MopupService.Instance.PopAsync();
    }

    private void DetailConfirmationYes_Clicked(object sender, EventArgs e)
    {
        MopupService.Instance.PushAsync(new CongratulationPopUp());
    }

}