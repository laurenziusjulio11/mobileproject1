namespace MobileProject.Views;

using Mopups.Services;

public partial class CongratulationPopUp 
{
	public CongratulationPopUp()
	{
		InitializeComponent();
	}

    private  async void CloseCongratulation_Clicked(object sender, EventArgs e)
    {
        MopupService.Instance.PopAsync();
        await Navigation.PushAsync(new HomePageAfterLogin());
    }
}