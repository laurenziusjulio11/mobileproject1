using MobileProject.ViewModels;
using System.Collections.ObjectModel;
using System.Net.Http.Json;
using System.Text.Json.Serialization;

namespace MobileProject.Views;

public partial class ListProject : ContentPage
{
    private readonly HttpClient httpClient = new();

    public bool IsRefreshing { get; set; }
    public ObservableCollection<Project> Projects { get; set; } = new();
    public Command RefreshCommand { get; set; }
    public Project SelectedProject { get; set; }

        public ListProject()
	    {


        RefreshCommand = new Command(async () =>
        {
            // Simulate delay
            await Task.Delay(2000);

            await LoadProjects();

            IsRefreshing = false;
            OnPropertyChanged(nameof(IsRefreshing));
        });

        

        BindingContext = this;



        InitializeComponent();
    }

    protected async override void OnNavigatedTo(NavigatedToEventArgs args)
    {
        base.OnNavigatedTo(args);

        await LoadProjects();
    }

    private void Button_Clicked(object sender, EventArgs e)
    {
        Projects.Clear();
    }

    private async Task LoadProjects()
    {
        var projects = await httpClient.GetFromJsonAsync<Project[]>("https://gitlab.com/bootcamp-batch-7/mobileproject/-/raw/main/projectdata.json?ref_type=heads");

        Projects.Clear();

        foreach (Project project in projects)
        {
            Projects.Add(project);
        }
    }

    private async void OnCounterClicked(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new JoinMember());
    }




}
