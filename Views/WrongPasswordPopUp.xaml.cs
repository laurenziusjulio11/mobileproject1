namespace MobileProject.Views;

using Mopups.Services;

public partial class WrongPasswordPopUp 
{
	public WrongPasswordPopUp()
	{
		InitializeComponent();
	}

    private async void WrongPassword_Clicked(object sender, EventArgs e)
    {
        MopupService.Instance.PopAsync();
    }
}

