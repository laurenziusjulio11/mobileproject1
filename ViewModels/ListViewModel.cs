using System.Collections.ObjectModel;
using System.ComponentModel;

namespace MobileProject.ViewModels;

public class ListViewModel : INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;

    private ObservableCollection<string> items;
    public ObservableCollection<string> Items
    {
        get => items;
        set
        {
            items = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Items)));
        }
    }

    private string selectedItem;
    public string SelectedItem
    {
        get => selectedItem;
        set
        {
            selectedItem = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedItem)));
        }
    }

    public ListViewModel()
    {
        // Initialize your list of items here
        Items = new ObservableCollection<string>
        {
            ".NET",
            "PHP",
            "JSON",
            "HTML"
            // Add more items as needed
        };
    }
}