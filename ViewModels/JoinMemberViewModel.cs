﻿using System;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Net.Http.Json;

namespace MobileProject.ViewModels
{
    public class JoinMemberViewModel : INotifyPropertyChanged
    {
        private readonly HttpClient httpClient = new HttpClient();
        private ObservableCollection<DetailProject> _detailProjects;
        private bool _isRefreshing;

        public ObservableCollection<DetailProject> DetailProjects
        {
            get => _detailProjects;
            set
            {
                _detailProjects = value;
                OnPropertyChanged();
            }
        }

        public bool IsRefreshing
        {
            get => _isRefreshing;
            set
            {
                _isRefreshing = value;
                OnPropertyChanged();
            }
        }

        public JoinMemberViewModel()
        {
            DetailProjects = new ObservableCollection<DetailProject>();
            IsRefreshing = false;
        }

        public async Task LoadDetailProjectsAsync()
        {
            try
            {
                IsRefreshing = true;

                var detailProjects = await httpClient.GetFromJsonAsync<DetailProject[]>("https://gitlab.com/bootcamp-batch-7/mobileproject/-/raw/main/projectdataPDetail.json");

                if (detailProjects != null)
                {
                    DetailProjects.Clear();

                    foreach (DetailProject detailProject in detailProjects)
                    {
                        DetailProjects.Add(detailProject);
                    }
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions, e.g., network errors
                Console.WriteLine($"Error loading detail projects: {ex}");
            }
            finally
            {
                IsRefreshing = false;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

